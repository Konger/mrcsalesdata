<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Amount Due Report</title>
<link href="//netdna.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- Include the fusioncharts core library -->
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!--  Include the fusion theme -->
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<script type="text/javascript">
	 

    // Chart Data
    const chartData = ${customerJSONArray};

    //Chart Configurations
    const chartConfig = {
    type: 'column2d',
    renderAt: 'chart-container',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
        // Chart Configuration
        "chart": {
            "caption": "Customer Payments Due Bar Chart",
            "xAxisName": "Customer",
            "yAxisName": "Receivable($)",
            "theme": "fusion",
            },
        // Chart Data
        "data": chartData
        }
    };
    FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts(chartConfig);
    fusioncharts.render();
    });

    //Strip the commas placed in by bootstrap
	$(document).ready(function(){
		$('.cnumber').each(function(i, obj) {
			var stripString = obj.innerHTML.replace(/,/g, '');
			obj.innerHTML=stripString;
		});
	});
</script>
</head>
<body>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-9">
       		<div class="justify-content-center container-fluid text-center"><h4>Customer Payments Due Table</h4><div>
        	<div class="table-responsive">
              <table class="table table-striped">
              	<thead>
                   <th>Customer Number</th>
                   <th>Customer Name</th>
                   <th>Amount Due(USD)</th>   
                </thead>
    			<tbody>
    				<#list customerList as customer>
					<tr>
						<td class="cnumber">${customer.customerNumber}</td>
						<td>${customer.customerName}</td>
						<td>$${customer.amountDue}</td>
					</tr>
					</#list>
    			</tbody>
       		</table>
			<div class="clearfix"></div>
          </div> 
        </div>
	</div>
</div>

<div class="container" style="margin-top:60px">
	<div class="row justify-content-center">
		<div id="chart-container" class="col-md-12"></div>
	</div>
</div>
</body>
</html>