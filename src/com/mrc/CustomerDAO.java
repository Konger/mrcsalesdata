package com.mrc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CustomerDAO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerDAO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public List<Customer> getAllCustomers() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Customer> customerList = new ArrayList<Customer>();
		String sql = "SELECT * FROM customers";
		
		try {
			conn = ConnectionFactory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				int number = rs.getInt("cnum");
				String name = rs.getString("cname");
				double amountDue = rs.getDouble("amtdu");
				Customer customer = new Customer(number, name, amountDue );
				customerList.add(customer);
			}
			rs.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			
			if(stmt != null)
			{
				stmt.close();
			}
			if(conn != null)
			{
				conn.close();
			}
		}
		
		return customerList;
		
	}

}
