package com.mrc;

public class Customer {
	
	private int customerNumber;
	private String customerName;
	private double amountDue;

	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	public Customer(int customerNumber, String customerName, double amountDue) {
		this.customerNumber = customerNumber;
		this.customerName = customerName;
		this.amountDue = amountDue;
	}
	
	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}


}
