package com.mrc;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.SimpleHash;


/**
 * Servlet implementation class SalesDataServlet
 */
@WebServlet("/index.html")
public class SalesDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String templateDir = "WEB-INF/templates";
	
	private FreemarkerTemplateProcessor processor;
	
	private CustomerDAO customerDAO;
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SalesDataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		processor = new FreemarkerTemplateProcessor( templateDir, getServletContext());
		customerDAO = new CustomerDAO();

		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Customer> customerList = null;
		String customerJSONArray = "[]";
		try {
			customerList = customerDAO.getAllCustomers();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		DefaultObjectWrapperBuilder db = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_29);
		SimpleHash root = new SimpleHash(db.build());
		root.put("customerList", customerList);
		
		if(customerList!= null && customerList.size()>0) {
			customerJSONArray = createCustomerListJSONArray(customerList);
		}
		root.put("customerJSONArray", customerJSONArray);
		
		String templateName = "index.ftl";
		processor.processTemplate(templateName, root, request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	

	@SuppressWarnings("unchecked")
	private String createCustomerListJSONArray( List<Customer> customers ) {
		JSONArray array = new JSONArray();
		for(Customer customer: customers) {
			JSONObject obj = new JSONObject();
			obj.put("label", customer.getCustomerName());
			obj.put("value", customer.getAmountDue());
			array.add(obj);
		}
		return array.toString();
	}
}
